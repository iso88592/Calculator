﻿namespace Calculator;

public class Szamologep
{
    public IEnumerable<string> Sugo()
    {
        return new[]
        {
            "h:\tsegítség",
            "q:\tkilépés",
            "+:\tösszeadás",
            "-:\tkivonás",
            "*:\tszorzás",
            "/:\tosztás",
            "%:\tmaradékos osztás",
            "<szám>:\tszám bevitele"
        };
    }

    public int Ertek { get; set; }

    public void MuveletBeallitas(string muvelet)
    {
        UtolsoErtek = Ertek;
        Muvelet = muvelet;
    }

    public string Muvelet { get; private set; } = "";

    public int UtolsoErtek { get; set; }

    public void Szamitas()
    {
        switch (Muvelet)
        {
            case "+":
                Ertek = UtolsoErtek + Ertek;
                break;
            case "-":
                Ertek = UtolsoErtek - Ertek;
                break;
            case "*":
                Ertek = UtolsoErtek * Ertek;
                break;
            case "/":
                Ertek = UtolsoErtek / Ertek;
                break;
            case "%":
                Ertek = UtolsoErtek % Ertek;
                break;
        }

        UtolsoErtek = Ertek;
    }
}