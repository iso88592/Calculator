﻿using Calculator;

var szamologep = new Szamologep();
var parancs = "";
Console.WriteLine("Milyen műveletet szeretnél elvégezni? (q: kilépés, h: segítség)");
while (parancs != "q")

{
    Console.Write(szamologep.Ertek + szamologep.Muvelet + " ");
    parancs = Console.ReadLine();
    switch (parancs)
    {
        case "":
            break;
        case "h":
            foreach (var line in szamologep.Sugo())
            {
                Console.WriteLine(line);
            }

            break;
        case "+":
            szamologep.MuveletBeallitas("+");
            break;
        case "-":
            szamologep.MuveletBeallitas("-");
            break;

        case "*":
            szamologep.MuveletBeallitas("*");
            break;
        case "/":
            szamologep.MuveletBeallitas("/");
            break;
        case "%":
            szamologep.MuveletBeallitas("%");
            break;
        case "q":
            break;
        default:
            if (int.TryParse(parancs, out int ertek))
            {
                szamologep.Ertek = ertek;
                szamologep.Szamitas();
            }
            else
            {
                Console.WriteLine("Ismeretlen parancs!");
            }

            break;
    }
}