using Calculator;

namespace CalculatorTest;

public class SzamologepTesztek
{

    [Test]
    public void KezdetbenASzamologepNullatMutat()
    {
        var szamologep = new Szamologep();
        Assert.AreEqual(0, szamologep.Ertek);
    }

    [Test]
    public void HaASzamologepKapEgySzamotAkkorAztASzamotMutatja()
    {
        var szamologep = new Szamologep();
        szamologep.Ertek = 12;
        szamologep.Szamitas();
        Assert.AreEqual(12, szamologep.Ertek);
    }
    
    [Test]
    public void HaASzamologepKapEgyMasikSzamotAkkorAMasodikSzamotMutatja()
    {
        var szamologep = new Szamologep();
        szamologep.Ertek = -81;
        szamologep.Szamitas();
        Assert.AreEqual(-81, szamologep.Ertek);
    }

    [Test]
    public void KetSzamotHelyesenAdOssze()
    {
        var szamologep = new Szamologep();
        szamologep.Ertek = 11;
        szamologep.MuveletBeallitas("+");
        szamologep.Ertek = 24;
        szamologep.Szamitas();
        Assert.AreEqual(35, szamologep.Ertek);
        
    }
    
}